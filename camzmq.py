import cv2
import numpy as np

import zmq

from imutils.video.pivideostream import PiVideoStream
import time

import config


# Initialize camera

vs = PiVideoStream(resolution=(1280, 720), framerate=60).start()
vs.camera.start_preview()

def update_camera_settings():
    vs.camera.brightness = config.settings["brightness"]
    vs.camera.contrast = config.settings["contrast"]
    vs.camera.shutter_speed = config.settings["shutter_speed"]
    vs.camera.awb_mode = config.settings["awb_mode"]
    vs.camera.awb_gains = (config.settings["awb_red"], config.settings["awb_blue"])

update_camera_settings()

time.sleep(0.5)




# Set up zmq socket
context = zmq.Context()

def create_socket(port):
    socket = context.socket(zmq.PUB)
    socket.set_hwm(1)
    socket.bind("tcp://*:" + str(port))
    return socket

raw_socket = create_socket(5000)
thresholded_socket = create_socket(5001)

def broadcast(img, socket):
    ret, buf = cv2.imencode(".jpg", img)
    socket.send(buf.tostring())


while True:
    if config.refresh_settings():
        update_camera_settings()

    img = vs.read()

    broadcast(img, raw_socket)

    threshold_value = config.settings["threshold_value"]

    if threshold_value != -1:
        _, thresholded = cv2.threshold(img[:,:,0], threshold_value, 255, 0)
        broadcast(thresholded, thresholded_socket)
