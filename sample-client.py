# To install zeromq on Mac:
#     brew install zeromq
#     pip install pyzmq

import zmq
import numpy as np
import cv2

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://rpicam2.local:5000")
socket.setsockopt(zmq.SUBSCRIBE, "")

while True:
    msg = socket.recv()

    image = np.frombuffer(buffer(msg), dtype=np.uint8)
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    cv2.imshow("Client", image)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
